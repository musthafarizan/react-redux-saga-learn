import TodoList from './components/TodoList';
import UserList from './components/UserList';
import './styles.css';

export default function App() {
  return (
    <div className="App">
      <h1>Users List</h1>
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-5">
            <UserList />
          </div>
          <div className="col-xs-7">
            <TodoList />
          </div>
        </div>
      </div>
    </div>
  );
}
