import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchTodos } from '../store/actions/todoActions';

function TodoList() {
  const { selectedUser, todos } = useSelector((state) => ({
    selectedUser: state.users.selectedUser,
    todos: state.todos.todos
  }));
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchTodos(selectedUser));
  }, [dispatch, selectedUser]);

  return (
    <>
      <h2>Todos</h2>
      <hr />
      <ul className="list-group text-left">
        {todos
          ?.sort((todo) => (todo?.completed ? 1 : -1))
          ?.map((todo) => (
            <li
              key={todo?.id}
              className={`list-group-item ${
                todo?.completed ? 'text-success' : 'text-danger'
              }`}
            >
              <i
                className={`glyphicon glyphicon-${
                  todo?.completed ? 'check' : 'unchecked'
                }`}
                style={{ display: 'inline-block', marginRight: 10 }}
              />
              {todo.title}
            </li>
          ))}
      </ul>
    </>
  );
}

export default TodoList;
