import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUsres, selectUser } from '../store/actions/userActions';

function UserList() {
  const { users, selectedUser } = useSelector((state) => state.users);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUsres());
  }, [dispatch]);

  const handleUserTileClick = (id) => () => {
    dispatch(selectUser(id));
  };

  return (
    <>
      <h2>Users</h2>
      <hr />
      {!users?.length ? (
        <h5 className="text-danger">There are no users to display</h5>
      ) : (
        <ul className="list-group text-left">
          {users.map((user) => (
            <li
              onClick={handleUserTileClick(user?.id)}
              key={user?.id}
              className={`list-group-item ${
                user?.id === selectedUser?.id ? 'active' : ''
              }`}
              role="button"
            >
              <i
                className="glyphicon glyphicon-user"
                style={{ display: 'inline-block', marginRight: 10 }}
              />
              {user?.name}
            </li>
          ))}
        </ul>
      )}
    </>
  );
}

export default UserList;
