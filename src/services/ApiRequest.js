async function request({ endPoint, query, method }) {
  const queryUrl = !query
    ? ''
    : Object.keys(query)
        .reduce((url, key) => `${url}&${key}=${query[key]}`, '?')
        .replace('&', '');
  const res = await fetch(
    `https://jsonplaceholder.typicode.com/${endPoint}${queryUrl}`,
    {
      method
    }
  );
  return await res.json();
}

async function get({ endPoint, query }) {
  return await request({ endPoint, query, method: 'GET' });
}

export default {
  request,
  get
};
