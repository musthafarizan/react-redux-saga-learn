import ApiRequest from './ApiRequest';

async function getByUser(userId) {
  try {
    return await ApiRequest.get({
      endPoint: 'todos',
      query: userId ? { userId } : undefined
    });
  } catch (ex) {
    return [];
  }
}

export default {
  getByUser
};
