import ApiRequest from './ApiRequest';

async function getAll() {
  try {
    return await ApiRequest.get({ endPoint: 'users' });
  } catch (ex) {
    return [];
  }
}

export default {
  getAll
};
