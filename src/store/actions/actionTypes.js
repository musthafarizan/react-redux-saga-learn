export const FETCH_USRES = 'FETCH_USRES';
export const SET_USRES = 'SET_USRES';
export const SELECT_USER = 'SELECT_USER';
export const SET_SELECTED_USER = 'SET_SELECTED_USER';

export const FETCH_TODOS = 'FETCH_TODOS';
export const SET_TODOS = 'SET_TODOS';
