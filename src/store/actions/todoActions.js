import { FETCH_TODOS } from './actionTypes';

export function fetchTodos(selectedUser) {
  return {
    type: FETCH_TODOS,
    payload: selectedUser?.id
  };
}
