import { FETCH_USRES, SELECT_USER } from './actionTypes';

export function fetchUsres() {
  return {
    type: FETCH_USRES
  };
}

export function selectUser(userId) {
  return {
    type: SELECT_USER,
    payload: userId
  };
}
