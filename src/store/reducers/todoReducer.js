import { SET_TODOS } from '../actions/actionTypes';

const initState = {
  todos: []
};

export default function (state = initState, action) {
  switch (action.type) {
    case SET_TODOS:
      return { ...state, todos: action.payload };
    default:
      return state;
  }
}
