import { SET_SELECTED_USER, SET_USRES } from '../actions/actionTypes';

const initState = {
  users: [],
  selectedUser: null
};

export default function (state = initState, action) {
  switch (action.type) {
    case SET_USRES:
      return { ...state, users: action.payload };
    case SET_SELECTED_USER:
      const user = state.users.find((u) => u?.id === action.payload);
      return { ...state, selectedUser: user };
    default:
      return state;
  }
}
