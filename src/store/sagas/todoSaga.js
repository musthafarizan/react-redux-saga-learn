import { all, call, put, takeLatest } from 'redux-saga/effects';
import TodoService from '../../services/TodoService';
import { SET_TODOS, FETCH_TODOS } from '../actions/actionTypes';

function* fetchTodos(action) {
  const todos = yield call(TodoService.getByUser, action.payload);
  yield put({ type: SET_TODOS, payload: todos });
}

export default function* watchTodoSagas() {
  yield all([takeLatest(FETCH_TODOS, fetchTodos)]);
}
