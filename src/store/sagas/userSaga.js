import { all, call, put, takeLatest } from 'redux-saga/effects';
import UserService from '../../services/UserService';
import {
  FETCH_USRES,
  SET_USRES,
  SELECT_USER,
  SET_SELECTED_USER
} from '../actions/actionTypes';

function* fetchUser() {
  const users = yield call(UserService.getAll);
  yield put({ type: SET_USRES, payload: users });
}

function* selectUser(action) {
  yield put({
    type: SET_SELECTED_USER,
    payload: action.payload
  });
}

export default function* watchUserSagas() {
  yield all([
    takeLatest(FETCH_USRES, fetchUser),
    takeLatest(SELECT_USER, selectUser)
  ]);
}
